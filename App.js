import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, Image, FlatList, Text, View, Button } from 'react-native';

export default function App() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [sort, setSort] = useState(def);
  let def = "def";
  let episodeAsc = "episodeAsc";
  let episodeDesc = "episodeDesc";

  const sorting = () => {
    let sortedData;

    if (sort == "episodeDesc" || sort == "def") {
      sortedData = data.sort((a, b) => a.episode_number - b.episode_number);
      setSort(episodeAsc);
    }
    else if (sort == "episodeAsc") {
      sortedData = data.sort((a, b) => b.episode_number - a.episode_number);
      setSort(episodeDesc);
    }
    
    setData(sortedData);
  }

  const getMovies = async () => {
     try {
      const response = await fetch('https://raw.githubusercontent.com/RyanHemrick/star_wars_movie_app/master/movies.json');
      const json = await response.json();
      setData(json.movies);
      setSort(def);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getMovies();
  }, []);

  const imgUrl = 'https://raw.githubusercontent.com/RyanHemrick/star_wars_movie_app/master/public/images/';

  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data}
          keyExtractor={({ episode_number }) => episode_number}
          renderItem={({ item }) => (
            <View style={styles.movies}>
              <Image
                style={styles.poster}
                source= {{
                  uri: imgUrl+item.poster,
                }}
              />
              <Text>{item.title}</Text>
            </View>
          )}
        />
      )}
      <View style={styles.buttonWrap}>
        <Button color='white' title='Seřadit dle epizod' onPress={sorting}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    alignItems: 'center',
  },
  movies: {
    flex: 1,
    alignItems: 'center',
    marginTop: 15,
  },
  poster: {
    width: 50,
    height: 80,
    marginBottom: 10,
  },
  buttonWrap: {
    width: '40%',
    backgroundColor: '#f47a00',
    marginTop: 20,
  },
});